import {
    ADD_PLATE,
    ADD_PLATE_CORRECTLY,
    ADD_PLATE_ERROR,

    SHOW_PLATES_BEGIN,
    SHOW_PLATES_CORRECTLY,
    SHOW_PLATES_ERROR,

    DELETE_PLATE_BEGIN,
    DELETE_PLATE_CORRECTLY,
    DELETE_PLATE_ERROR,

    GET_EDIT_PLATE,
    EDIT_PLATE_BEGIN,
    EDIT_PLATE_CORRECTLY,
    EDIT_PLATE_ERROR
} from '../types';

import axiosClient from '../config/axios';
import Swal from 'sweetalert2';

// Create new plates
export function createNewPlateAction(plate) {
    return async (dispatch) => {
        dispatch( addPlate()  );

        try {
            // insert in the API
            await axiosClient.post('/plates', plate);

            // it is ok? update the state
            dispatch( addPlateCorrectly(plate) )
            // Success alert
            Swal.fire(
                'Success',
                'The plate has been added successfuly',
                'success'
            )

        } catch (error) {
            console.log(error);
            // if there is a error change the state
            dispatch( addPlateError(true) )

            // Error Alert
            Swal.fire({
                icon: 'error',
                title: 'We have an error',
                text: 'There was an error inserting the plate, try again please'
            })
        }
    }
}
// New Plates functions 
const addPlate = () => ({
    type: ADD_PLATE,
    payload: true
})

//if the plate save successfully in BD
const addPlateCorrectly = (plate) => ({
    type: ADD_PLATE_CORRECTLY,
    payload: plate
})

// if the plate save with errors
const addPlateError = (addPlateState) => ({
    type: ADD_PLATE_ERROR,
    payload: addPlateState
})

// Get plates from BD
export function getPlatesAction() {
    return async (dispatch) => {
        dispatch( getPlates() ); 

        try {
            const response = await axiosClient.get('/plates');
            dispatch( getPlatesCorrectly(response.data) );
        } catch (error) {
            dispatch( getPlatesError() );
        }
    }
}
const getPlates = () => ({
    type: SHOW_PLATES_BEGIN,
    payload: true
});
const getPlatesCorrectly = (plates) => ({
    type: SHOW_PLATES_CORRECTLY,
    payload: plates
});
const getPlatesError = () => ({
    type: SHOW_PLATES_ERROR,
    payload: true
})

// Delete a plate from BD
export function deletePlateAction(id) {
    return async (dispatch) => {
        dispatch( deletePlate(id) );
        try {
            await axiosClient.delete(`/plates/${id}`);
            dispatch( deletePlateCorrectly() ); 
            // If it is deleted show alert
            Swal.fire(
                'Deleted!',
                'Your plate has been deleted.',
                'success'
            )   
        } catch (error) {
            console.log(error);
            dispatch ( deletePlateError() );
        }
    }
}
const deletePlate = (id) => ({
    type: DELETE_PLATE_BEGIN,
    payload: id
});

const deletePlateCorrectly = () => ({
    type: DELETE_PLATE_CORRECTLY
})

const deletePlateError = () => ({
    type: DELETE_PLATE_ERROR,
    payload: true
})

export function getEditPlateAction(plate) {
    return (dispatch) => {
        dispatch ( getEditPlate(plate) )
    }
}
const getEditPlate = (plate) => ({
    type: GET_EDIT_PLATE,
    payload: plate
})

export function editPlateAction(plate){
    return async (dispatch) => {
        dispatch( editPlate() )
        console.log('Edit Plate id', plate.id);

        try {
            await axiosClient.put(`/plates/${ plate.id }`, plate);
            dispatch( editPlateCorrectly(plate) )
        } catch (error) {
            console.log(error);
            dispatch( editPlateError() )
        }
    }
}
const editPlate = () => ({
    type: EDIT_PLATE_BEGIN
})
const editPlateCorrectly = (plate) => ({
    type: EDIT_PLATE_CORRECTLY,
    payload: plate
});
const editPlateError = () => ({
    type: EDIT_PLATE_ERROR,
    payload: true
})
