import React from 'react';
import Header from './components/Header';
import Plates from './components/Plates';
import NewPlate from './components/NewPlate'
import EditPlate from './components/EditPlate'

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'; 
// redux
import { Provider } from 'react-redux';
import store from './store';

function App() {
  return (
    <Router>
      <Provider store={ store } >
        <Header />
        <div>
          <Switch>
            <Route exact path="/" component={ Plates } />
            <Route exact path="/plates/new" component={ NewPlate } />
            <Route exact path="/plates/edit/:id" component={ EditPlate } /> 
          </Switch>
        </div>
      </Provider>
    </Router>
  );
}

export default App;
