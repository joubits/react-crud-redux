import {
    ADD_PLATE,
    ADD_PLATE_CORRECTLY,
    ADD_PLATE_ERROR,

    SHOW_PLATES_BEGIN,
    SHOW_PLATES_CORRECTLY,
    SHOW_PLATES_ERROR,

    DELETE_PLATE_BEGIN,
    DELETE_PLATE_CORRECTLY,
    DELETE_PLATE_ERROR,

    GET_EDIT_PLATE,
    EDIT_PLATE_CORRECTLY,
    EDIT_PLATE_ERROR
} from '../types';

// each reducer has its own state
const initialState = {
    plates: [],
    error: null,
    loading: false,
    platetodelete: null,
    platetoedit: null
}
// modify the state
export default function(state = initialState, action) {
    switch(action.type) {
        case ADD_PLATE: 
        case SHOW_PLATES_BEGIN:
            return {
                ...state,
                loading: action.payload
            }
        case ADD_PLATE_CORRECTLY: 
            return {
                ...state,
                loading: false,
                plates: [...state.plates, action.payload]
            }
        case ADD_PLATE_ERROR:
        case SHOW_PLATES_ERROR:
        case DELETE_PLATE_ERROR:
        case EDIT_PLATE_ERROR:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case SHOW_PLATES_CORRECTLY:
            return {
                ...state,
                loading: false,
                error: null,
                plates: action.payload
            }

        case DELETE_PLATE_BEGIN:
            return {
                ...state,
                platetodelete: action.payload
            }
        
        case DELETE_PLATE_CORRECTLY:
            return {
                ...state,
                plates: state.plates.filter( plate => plate.id !== state.platetodelete ),
                platetodelete: null
            }

        case GET_EDIT_PLATE:
            return {
                ...state,
                platetoedit: action.payload
            }
        case EDIT_PLATE_CORRECTLY:
            return {
                ...state,
                platetoedit: null,
                plates: state.plates.map( (plate) => 
                    plate.id === action.payload.id ? plate = action.payload : plate
                )
            }
        
        
        default:
            return state;
        
    }
}