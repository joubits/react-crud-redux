import { combineReducers } from 'redux';
import platesReducer from './platesReducer';
import alertReducer from './alertReducer'

export default combineReducers({
    plates: platesReducer,
    alert: alertReducer
})