import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Message from './shared/Message';

import { editPlateAction } from '../actions/plateActions';
import { showAlertAction, hideAlertAction } from '../actions/alertActions';

const EditPlate = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    // new state of plate
    const [ plate, savePlate ] = useState({
        name: '',
        description: ''
    })
    
    // plate to edit
    const platetoedit = useSelector( state => state.plates.platetoedit );
    const alert = useSelector( state => state.alert.alert );
    
    useEffect( () => {
        savePlate(platetoedit);
    }, [platetoedit]);

    //get data from form
    const onChangeForm = e => {
        savePlate({
            ...plate,
            [e.target.name]: e.target.value
        })
    }

    const { name, description } = plate;

    const submitEditPlate = (e) => {
        e.preventDefault();
        //validate form
        if( name.trim()==='' || description.trim()===''){
            const alert = {
                message: "You are editing a plate, please fill the mandatory fields",
                classes: "ui negative message"
            }
            dispatch( showAlertAction(alert) );
            return;
        }

        // if no errors
        dispatch( hideAlertAction() );

        // send to the Action
        dispatch( editPlateAction(plate) );
        history.push('/');
    }

    return (
        <div className="ui container">
            <h2> Edit Plate </h2>
            <Message alert={alert} />
            <form className="ui form"
                    onSubmit={submitEditPlate}>
                <div className="field">
                    <label>Plate Name</label>
                    <input type="text" name="name" 
                            placeholder="Enter plate name"
                            value={ name } onChange={ onChangeForm } />
                </div>
                <div className="field">
                    <label>Plate Description</label>
                    <input type="text" name="description" 
                            placeholder="Enter plate description" 
                            value={ description } onChange={ onChangeForm } />
                </div>
                <button className="ui primary button" type="submit">Add Plate</button>
            </form>
        </div>
    )
}

export default EditPlate;
