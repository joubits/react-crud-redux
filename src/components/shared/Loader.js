import React from 'react'

export default function Loader(props) {
    const { loading } = props;
    return (
        <div>
            { loading ? <div className="ui segment">
                            <div className="ui active inverted dimmer"><div className="ui text loader">Loading</div></div>
                        </div> : null 
            }
        </div>
    )
}
