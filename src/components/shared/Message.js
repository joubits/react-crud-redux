import React from 'react'

export default function Message(props) {
    return (
        <div>
            { props.alert ? <div className={props.alert.classes}>
                <div className="header">{ props.alert.message }</div>
            </div> : null }
            
            { props.error ? <div class="ui segment">
                                <div class="ui active inverted dimmer"><div class="ui text loader">Loading</div></div>
                            </div> 
                            : null }
        </div>
    )
}
