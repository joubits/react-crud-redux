import React from 'react';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';

// redux
import { useDispatch } from 'react-redux';
import { deletePlateAction, getEditPlateAction } from '../actions/plateActions';

const Plate = ( {plate} ) => {
    const { name, description, id } = plate;

    const dispatch = useDispatch();
    const history = useHistory(); // enable history for redirect...

    // confirm to delete plate
    const confirmDeletePlate = id => {
        // confirm to delete
        Swal.fire({
            title: 'Are you sure?',
            text: "A deleted plate won't be able to revert!!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'Cancel'
        }).then((result) => {
            if (result.value) {
                // send to the Action
                dispatch( deletePlateAction(id) );
            }
        })
    }

    // function to a programmed redirect
    const redirectEdit = (plate) => {
        dispatch ( getEditPlateAction(plate) );
        history.push(`/plates/edit/${plate.id}`);
    }

    return (
        <tr>
            <td className="left aligned">{name}</td>
            <td className="left aligned">{description}</td>
            <td className="left aligned">
                <button type="button" className="blue ui button" onClick={ () => redirectEdit(plate) }>Edit</button>
                <button type="button" className="red ui button" onClick={ () => confirmDeletePlate(id) }>Delete</button>
            </td>
        </tr>
    )
}

export default Plate;
