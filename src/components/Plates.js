import React, { Fragment, useEffect } from 'react';
import Plate from './Plate';
import Message from './shared/Message';
import Loader from './shared/Loader';

// Redux
import { useSelector, useDispatch } from 'react-redux';
import { getPlatesAction } from '../actions/plateActions';

const Plates = () => {
    const dispatch = useDispatch();

    useEffect( () => {
        // get data api
        const getPlates = () => dispatch( getPlatesAction() );
        getPlates();
        // eslint-disable-next-line
    }, []);

    // get the state
    const plates = useSelector( state => state.plates.plates );
    const error = useSelector( state => state.plates.error );
    const loading = useSelector ( state => state.plates.loading);

    // fix to add recent inserted plate
    if(plates.length > 0 && plates[plates.length - 1].id === undefined) {
        plates[plates.length - 1].id = plates[plates.length - 2].id + 1;
    }

    return (
        <Fragment>
            <div className="ui container one column relaxed grid">
                <div className="column">
                    <h2>List of Plates</h2> 
                    <Message error={error} />
                    <Loader loading={loading} />
                    <table className="ui right aligned table">
                        <thead>
                            <tr>
                                <th className="left aligned" scope="col">Name</th>
                                <th className="left aligned" scope="col">Description</th>
                                <th className="left aligned" scope="col">Actions</th> 
                            </tr>
                        </thead>
                        <tbody>
                            { plates.length === 0 ? 'No plates' : (
                                plates.map( plate => (
                                    <Plate 
                                        key={ plate.id ? plate.id : plate.id - 1 }
                                        plate = { plate }
                                    />
                                ) )
                            ) }
                        </tbody>
                    </table>
                </div>
            </div>
        </Fragment>
    )
}

export default Plates;
