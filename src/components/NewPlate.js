import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Message from './shared/Message';
import Loader from './shared/Loader';

// Redux Actions
import { createNewPlateAction } from '../actions/plateActions';
import { showAlertAction, hideAlertAction } from '../actions/alertActions';

const NewPlate = ({ history }) => {
    // component state
    const [name, saveName] = useState('');
    const [description, saveDescription] = useState('');
    
    // use useDispatch and create a function
    const dispatch = useDispatch();

    // access to store state with useSelector
    const loading = useSelector(state => state.plates.loading);
    const error = useSelector(state => state.plates.error);
    const alert = useSelector( state => state.alert.alert );

    // call Action of plateAction
    const addPlate = (plate) => dispatch( createNewPlateAction(plate) );

    // when the user submits the form
    const submitNewPlate = e => {
        e.preventDefault();
        // validate form
        if(name.trim() === '' || description.trim() === '') {
            const alert = {
                message: 'Both fields are obligatory',
                classes: 'ui negative message'
            };
            dispatch( showAlertAction(alert) );
            return;
        }

        // if no errors
        dispatch( hideAlertAction() );

        //create the new plate
        addPlate({
            name,
            description
        });
        //redirect after added the plate
        history.push('/');
    }
    return (
        <div className="ui container">
            <h2> Add New Plate </h2>
            <Message alert={alert} />
            <form className="ui form"
                    onSubmit={ submitNewPlate }>
                <div className="field">
                    <label>Plate Name</label>
                    <input type="text" name="name" 
                            placeholder="Enter plate name"
                            value={ name } onChange={e => saveName(e.target.value)} />
                </div>
                <div className="field">
                    <label>Plate Description</label>
                    <input type="text" name="description" 
                            placeholder="Enter plate description" value={ description } 
                            onChange={e => saveDescription(e.target.value)} />
                </div>
                <button className="ui primary button" type="submit">Add Plate</button>
            </form>
            <Loader loading={loading} />
            <Message error={error} />
        </div>

    )
}

export default NewPlate;
