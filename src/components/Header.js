import React from 'react';
import { Link } from 'react-router-dom'; 

const Header = () => {
    return (
        <div className="ui menu">
            <div className="header item"><Link to={'/'}>React, Redux - Plates CRUD</Link></div>
            <div className="right menu">
                <Link to={"/plates/new"} className="item">Add Plate &#43;</Link>
            </div>
        </div>
    )
}

export default Header;
